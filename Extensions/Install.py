from cStringIO import StringIO
from Products.Archetypes.Extensions.utils import install_subskin
from Products.PluggableAuthService.interfaces.plugins \
     import IExtractionPlugin, \
            IAuthenticationPlugin, \
            ICredentialsResetPlugin, \
            IGroupsPlugin, \
            IPropertiesPlugin
from Products.McAuthPlugin.config import PROJECTNAME, GLOBALS

def install(self):
    out = StringIO()

    install_subskin(self, out, GLOBALS)

    mcauths = self.acl_users.objectIds('McAuth Authentication Helper')
    if 0 == len(mcauths):
        raise AttributeError("No McAuth plugin in the acl_users folder.")
    elif 1 < len(mcauths):
        print >> out, "WARNING: more than one McAuth plugin, configuring for the first one (%s)." % (mcauths[0], )

    mcauth = mcauths[0]
    plugins = self.acl_users.plugins
    for interface in (IExtractionPlugin,
                      IAuthenticationPlugin,
                      ICredentialsResetPlugin,
                      IGroupsPlugin,
                      IPropertiesPlugin,
                      ):
        if mcauth not in plugins.listPluginIds( interface ):
            plugins.activatePlugin( interface, mcauth )
            print >> out, "Registered interface %s of McAuth plugin %s." \
                  % (interface.__name__, mcauth)
        else:
            print >> out, "No need to register interface %s of McAuth plugin %s." \
                  % (interface.__name__, mcauth)
    
    print >> out, "Installation completed."
    return out.getvalue()

