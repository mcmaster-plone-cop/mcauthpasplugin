"""McAuthPlugin
Copyright(C), 2006, Career Services, McMaster University - ALL RIGHTS RESERVED

This software is licensed under the Terms and Conditions contained within the
LICENSE.txt file that accompanied this software.  Any inquiries concerning the
scope or enforceability of the license should be addressed to:

Career Services, McMaster University
1280 Main Street Wes, Gilmour Hall 110
Hamilton, Ontario L8S 4L8 Canada
phone: +1 905 525-9140 x 24254
http://careers.mcmaster.ca
"""

from AccessControl import ClassSecurityInfo

from Globals import InitializeClass

from Products.PluggableAuthService.plugins.BasePlugin import BasePlugin
from Products.PluggableAuthService.utils import classImplements
from Products.PluggableAuthService.interfaces.plugins \
     import IExtractionPlugin, \
            IAuthenticationPlugin, \
            ICredentialsResetPlugin, \
            IPropertiesPlugin, \
            IGroupsPlugin

from Products.PlonePAS.sheet import MutablePropertySheet

from Products.PageTemplates.PageTemplateFile import PageTemplateFile

from Products.SessionCrumbler.sessionlazycheck import hasSession

from Crypto.Cipher import DES3

from DateTime import DateTime

from urllib import urlencode

import binascii

import cStringIO, gzip

manage_addMcAuthPluginForm = PageTemplateFile(
    'www/mcAuthAdd', globals(), __name__='manage_addMcAuthPluginForm' )

def manage_addMcAuthPlugin(dispatcher, id,
                           applicationId, applicationName,
                           encryptionKey, encryptionIV, authServerURL,
                           version='v2',
                           tokenName=None, sessionKey=None, timeout=None,
                           REQUEST=None, title=None):
    """ Add a McAuthPlugin to a Pluggable Auth Service."""

    obj = McAuthPlugin(id,
                       applicationId, applicationName,
                       encryptionKey, encryptionIV, authServerURL,
                       title, version, tokenName, sessionKey, timeout,
                       )
    dispatcher._setObject(obj.getId(), obj)

    if REQUEST is not None:
        REQUEST['RESPONSE'].redirect(
                                '%s/manage_workspace'
                                '?manage_tabs_message='
                                'McAuthPlugin+added.'
                            % dispatcher.absolute_url())

# Used by the h_decode function
packmap = [
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,

    0x00,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,

    0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x00,0x01,0x02,0x03,0x0b,0x0c,0x0d,0x0e,0x0f,

    0x00,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,

    0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,0x00,0x01,0x02,0x03,0x0b,0x0c,0x0d,0x0e,0x0f,

    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f,
    ];

def h_decode(coded):
    """ Decode a string. For charset [0-9a-fA-F] it is equivalent to
    binascii.unhexlify.
    """
    return ''.join([chr( packmap[ord(coded[ ndx * 2])] << 4 |
                         packmap[ord(coded[ ndx * 2 + 1])] )
                    for ndx in range(len(coded) / 2)])

class McAuthPlugin(BasePlugin):

    """ PAS plugin for using McID credentials to authenticate.

    Supports McAuth 1 and 2.
    """

    meta_type = 'McAuth Authentication Helper'

    security = ClassSecurityInfo()

    manage_options = ( BasePlugin.manage_options )

    supported_versions = ('v1', 'v2')

    _properties = ({ 'id': 'title',
                     'label': 'Title',
                     'type': 'string',
                     'mode': 'w'},
                   { 'id': 'version',
                     'label': 'McAuth version',
                     'type': 'selection',
                     'select_variable': 'supported_versions',
                     'mode': 'w'},
                   { 'id': 'tokenName',
                     'label': 'Token name',
                     'type': 'string',
                     'mode': 'w'},
                   { 'id': 'timeout',
                     'label': 'Timeout for the authentication token in seconds',
                     'type': 'int',
                     'mode': 'w'},
                   { 'id': 'sessionKey',
                     'label': 'Session key for authentication data',
                     'type': 'string',
                     'mode': 'w'},
                   { 'id': 'applicationId',
                     'label': 'Application Id',
                     'type': 'int',
                     'mode': 'w'},
                   { 'id': 'applicationName',
                     'label': 'Application name',
                     'type': 'string',
                     'mode': 'w'},
                   # encryptionKey is r/w since the crypto object
                   # cannot be pickled, so cannot be stored as part of
                   # the authentication plugin in the ZODB
                   { 'id': 'encryptionKey',
                     'label': 'Encryption key',
                     'type': 'string',
                     'mode': 'w'},
                   { 'id': 'encryptionIV',
                     'label': 'Encryption initialization vector',
                     'type': 'string',
                     'mode': 'w'},
                   { 'id': 'authServerURL',
                     'label': 'Authentication server URL',
                     'type': 'string',
                     'mode': 'w'},
                   )

    def __init__(self, id, applicationId, applicationName,
                 encryptionKey, encryptionIV, authServerURL,
                 title=None, version='v2', tokenName='token',
                 sessionKey='__mcauth', timeout=1800):
        self._id = self.id = id
        self.title = title
        self.version = version
        self.tokenName = tokenName
        self.sessionKey = sessionKey
        self.timeout = timeout
        self.applicationId = applicationId
        self.applicationName = applicationName
        self.encryptionKey = encryptionKey
        self.encryptionIV = encryptionIV
        self.authServerURL = authServerURL

    security.declarePublic('authenticationURL')
    def authenticationURL(self):
        """Builds the authentication URL pointing to the
        authentication form.
        """
        
        return "%s?%s" % (self.authServerURL,
                          urlencode({'app_id':   self.applicationId,
                                     'app_name': self.applicationName,
                                     },
                                    ))

    security.declarePrivate('decodeData')
    def decodeData(self, data):
        """Decode data sent by the McAuth server.
        """

        if self.version == 'v1':
            cd = DES3.new(h_decode(self.encryptionKey.ljust(16*3)),
                          DES3.MODE_ECB)
        elif self.version == 'v2':
            cd = DES3.new(self.encryptionKey,
                          DES3.MODE_CBC,
                          self.encryptionIV
                          )
        else:
            raise ValueError("Invalid McAuth version number.")

        # As h_decode is equivalent to binascii.unhexlify for the
        # hexadecimal charset, this will work for both v1 and v2.
        decrypted = cd.decrypt(h_decode(data))
        
        if self.version == 'v2':
            # Unpad data using padding from PCKS#5/7
            decrypted = decrypted[:-ord(decrypted[-1])]
            gzstream = gzip.GzipFile('', 'r', 0, cStringIO.StringIO(decrypted))
            try:
                decoded = gzstream.read()
            except IOError:
                if 0 < gzstream.extrasize:
                    decoded = gzstream.extrabuf
                else:
                    raise
            return decoded
        else:
            return decrypted.strip()

    security.declarePrivate('extractCredentials')
    def extractCredentials(self, request):
        """ Extract the credentials from the token returned by the
        McAuth server.
        """
        
        response = request['RESPONSE']
        session_p = hasSession(self)

        try:
            # Let us assume we have received the authentication token
            token = request[self.tokenName]
            
            # Make the request look like it was generated in a
            # Zope/Plone form, else it is rejected
            request.form['form.submitted'] = 1
            request.form['js_enabled'] = 1
            request.form['cookies_enabled'] = 1
            # Recover the came_from variable that was saved before redirecting to McAuth
            try:
                if session_p:
                    request.form['came_from'] = request.SESSION['came_from']
            except KeyError:
                pass

            decoded = self.decodeData(token).split(':')
            
            # No element of the token can be empty
            if '' in decoded:
                return {}

            (mcauth_version,
             session_id,
             time_stamp,
             ip,
             user_id,
             auth_answer) = decoded
            auth_answer = auth_answer.lower() == 'yes'

            if not auth_answer:
                return {}
            
            # XXX: This probably doesn't work for proxied/SNATted clients/servers

            # XXX: check HTTP_X_FORWARDED_FOR?

            # Check if credentials where issued for this client
            #if self.checkClientIP and request.getClientAddr() != ip:
            #    self.resetCredentials(request, response)
            #    return {}

            # Check if token has expired
            if  int(self.timeout) < int(DateTime()) - int(time_stamp):
                return {}

            mcauth_data = {
                'authT': {
                    'mcauth_version': mcauth_version,
                    'session_id':     session_id,
                    'ip':       ip,
                    'user_id':  user_id,
                    'isAuth':   auth_answer,
                    },
                }
            
            # Extract the authorization information, if any
            try:
                decoded = self.decodeData(request.form['authz'])
                authz = {}
                for record in decoded.split("!"):
                    table, data = record.split("=")
                    data = dict(map(lambda s: s.split(":"),
                                    data.split(";")
                                    ))
                    try:
                        authz[table].append(data)
                    except KeyError:
                        authz[table] = [data]
                mcauth_data.update(authz)
            except KeyError:
                pass
            
            request.SESSION.set(self.sessionKey, mcauth_data)
            
            # Delete came_from key of the session. It must be done
            # here to ensure that it is not deleted in one of the
            # satellite requests for images, stylesheets or something
            # else.
            try: request.SESSION.delete("came_from")
            except: pass
            
            return {
                'user_id': user_id,
                'isAuth': auth_answer,
                }
        except KeyError:
            # No authentication token, lets check if the person is
            # already authenticated

            # Lets handle the came_from disfunction caused by McAuth
            # not forwarding any request variables
            if request.form.has_key("came_from"):
                request.SESSION.set("came_from",
                                    request.form["came_from"])

            # No session, no auth.
            if not session_p: return {}

            try:
                mcauth_data = request.SESSION[self.sessionKey]
                return {
                    'user_id': mcauth_data["authT"]["user_id"],
                    'isAuth': True,
                    }
            except KeyError:
                return {}

    security.declarePrivate('authenticateCredentials')
    def authenticateCredentials(self, credentials):
        """ See IAuthenticationPlugin.

        - We expect the credentials to be those returned by
          IExtractionPlugin.
        """

        if credentials['extractor'] != self.getId():
            return None, None

        user_id = credentials.get( 'user_id' )
        isAuth = credentials.get( 'isAuth' )

        if user_id is None:
            return None, None

        if not isAuth:
            return None, None

        return user_id, user_id

    security.declarePrivate( 'resetCredentials' )
    def resetCredentials( self, request, response ):
        """ User logged out: clean up!
        """
        
        if hasSession(self):
            request.SESSION.delete(self.sessionKey)
    
    security.declarePrivate( 'getGroupsForPrincipal' )
    def getGroupsForPrincipal( self, principal, request=None ):
        """ principal -> ( group_1, ... group_N )

        o Return a sequence of group names to which the principal 
          (either a user or another group) belongs.

        o May assign groups based on values in the REQUEST object, if present
        """

        if self.version == 'v1':
            return ()
        
        if request is None:
            return ()
        
        if not hasSession(self):
            return ()
        
        groups = ()
        try:
            mcauth_data = request.SESSION[self.sessionKey]
            if mcauth_data['authT']['user_id'] != principal.getId():
                return ()

            employee_records = mcauth_data.get("authzEm", []) + \
                               [ record for record in mcauth_data.get("authz", [])
                                 if record["applicTypeCode"] == "EM" ]
            if 0 < len(employee_records):
                groups += ('Employees', )
            
            student_records = mcauth_data.get("authzStPgm", []) + \
                              mcauth_data.get("authzStCrs", []) + \
                              [ record for record in mcauth_data.get("authz", [])
                                 if record["applicTypeCode"] == "ST" ]
            if 0 < len(student_records):
                groups += ('Students', )
        except KeyError:
            pass

        return groups
    
    security.declarePrivate( 'getPropertiesForUser' )
    def getPropertiesForUser( self, user, request=None ):
        """ user -> {}

        o User will implement IPropertiedUser.

        o Plugin may scribble on the user, if needed (but must still
          return a mapping, even if empty).

        o May assign properties based on values in the REQUEST object, if
          present
        """

        if request is None:
            return {}
        
        if self.version == 'v1':
            return {}
        
        if not hasSession(self):
            return {}

        properties = {}
        try:
            mcauth_data = request.SESSION[self.sessionKey]
            if mcauth_data['authT']['user_id'] != user.getId():
                return {}

            properties['firstname'] = " ".join((mcauth_data["authz"][0]['fgivenameName'],
                                                mcauth_data["authz"][0]['lgivenameName']))
            properties['lastname'] = mcauth_data["authz"][0]['surnameName']
            properties['fullname'] = " ".join((mcauth_data["authz"][0]['fgivenameName'],
                                               mcauth_data["authz"][0]['lgivenameName'],
                                               mcauth_data["authz"][0]['surnameName']))
            properties['email'] = "%s@mcmaster.ca" % (mcauth_data['authT']['user_id'], )
        except KeyError:
            pass

        return MutablePropertySheet(self.id, **properties)


classImplements(McAuthPlugin,
                IExtractionPlugin,
                IAuthenticationPlugin,
                ICredentialsResetPlugin,
                IGroupsPlugin,
                IPropertiesPlugin,
                )

InitializeClass(McAuthPlugin)
