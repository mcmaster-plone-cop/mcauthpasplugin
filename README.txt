Description

    McAuthPlugin is a Pluggable Authentication Source plugin
    implementing the McAuth authentication.

    It is based on the GMailAuthPlugin by J Cameron Cooper
    <plone@jcameroncooper.com>.

Installation

    Place the Product directory 'McAuthPlugin' in your 'Products/'
    directory and restart the server.

    In your PAS 'acl_users', select "McAuth Authentication Helper"
    from the add list. Give it an id and title, and click on the add
    button.

    Enable the Authentication plugin interface in the after-add
    screen.

    Go to the 'Site Setup' page in the Plone interface and click on
    the 'Add/Remove Products' link. Choose McAuthPlugin (check its
    checkbox) and click the 'Install' button. This will make the
    custom login form providing with a link for McAuth login, take
    priority over the standard Plone login form.

    You may have to empty your browser cache to see the effects of the
    product installation/uninstallation.

    If you have users in another user source that may have the same
    name as a McID login, and you want the McAuth login to take
    precedence (usually this is not the case!), you should visit the
    'plugins' plugin and rearrange the order in the 'Authentication
    Plugins'.

TODO

    - Implement next version of McAuth

Author

  Servilio Afre Puentes <afrepues@cpec.admin.mcmaster.ca>
