"""McAuthPlugin
Copyright(C), 2006, Career Services, McMaster University - ALL RIGHTS RESERVED

This software is licensed under the Terms and Conditions contained within the
LICENSE.txt file that accompanied this software.  Any inquiries concerning the
scope or enforceability of the license should be addressed to:

Career Services, McMaster University
1280 Main Street Wes, Gilmour Hall 110
Hamilton, Ontario L8S 4L8 Canada
phone: +1 905 525-9140 x 24254
http://careers.mcmaster.ca
"""

from AccessControl.Permissions import add_user_folders
from Products.PluggableAuthService.PluggableAuthService import registerMultiPlugin
from McAuthPlugin import McAuthPlugin, manage_addMcAuthPlugin, manage_addMcAuthPluginForm
from Products.CMFCore.DirectoryView import registerDirectory
from config import GLOBALS
import Zope2, logging

registerDirectory('skins', GLOBALS)
registerDirectory('skins/mcauthplugin', GLOBALS)


def initialize(context):
    """ Initialize the McAuthPlugin """
    registerMultiPlugin(McAuthPlugin.meta_type)

    context.registerClass( McAuthPlugin
                           , permission=add_user_folders
                           , constructors=( manage_addMcAuthPluginForm
                                            , manage_addMcAuthPlugin
                                            )
                           , icon='www/mcmaster.gif'
                           , visibility=None
                           )
    # Updating the already existing instances
    logger = logging.getLogger("McAuth")
    zope_app = Zope2.app()
    paths = set()
    
    for path, plugin in zope_app.ZopeFind( zope_app,
                                           obj_metatypes=[McAuthPlugin.meta_type],
                                           search_sub = 1
                                           ):
        if hasattr(plugin, "cookieName"):
            plugin.sessionKey = plugin.cookieName
            del plugin.cookieName
            paths.add(path)
        if not hasattr(plugin, "encryptionIV"):
            plugin.encryptionIV = ''
            paths.add(path)
        if not hasattr(plugin, "version"):
            plugin.version = 'v2'
            paths.add(path)

    if len(paths):
        logger.info("Migrated the plugin(s) at %s" % (", ".join(paths), ))
    else:
        logger.info("No plugins needed migration.")
