#
# McAuthTestCaseBase, derived from PlonePASTestCase
#

import os, sys
import unittest

from Testing import ZopeTestCase

from zope.app.testing.placelesssetup import setUp, tearDown
from Products.Five import zcml
from Products.PloneTestCase import PloneTestCase
from Products.PloneTestCase.setup import PLONE25
import Products.statusmessages

from Products.PloneTestCase.PloneTestCase import FunctionalTestCase

from Products.McAuthPlugin import McAuthPlugin
from Products.McAuthPlugin.McAuthPlugin import h_decode
from binascii import unhexlify, hexlify
from Crypto.Cipher import DES3
import time
import cStringIO, gzip

# Stub test case just in case we need custom stuff at some point
class McAuthTestCaseBase(McAuthPlugin, PloneTestCase.PloneTestCase):
    """TestCase base for McAuth. This class define support functions
    for building the tests, like the generation of valid encoded tokens.
    """

    def __init__(self, version, key, iv, url = '%s'):
        McAuthPlugin.__init__(self, "mcauth_helper", 0, "mcauth-test",
                              key, iv, "%s",
                              version = version,
                              )
        self.url = url

    def encodeData(self, data):
        """Encode data sent by the McAuth server.
        """

        if self.version == 'v2':
            # Unpad data using padding from PCKS#5/7
            data_stream = cStringIO.StringIO()
            gzstream = gzip.GzipFile('', 'wb', 0, data_stream)
            gzstream.write(data)
            gzstream.close()
            data = data_stream.getvalue()

        padding = 8 - (len(data) % 8)
        if self.version == "v2":
            fillchar = chr(padding)
        else:
            fillchar = " "

        data = data.ljust(len(data) + padding, fillchar)

        if self.version == 'v1':
            cd = DES3.new(h_decode(self.encryptionKey.ljust(16*3)),
                          DES3.MODE_ECB)
        elif self.version == 'v2':
            cd = DES3.new(self.encryptionKey,
                          DES3.MODE_CBC,
                          self.encryptionIV
                          )
        else:
            raise ValueError("Invalid McAuth version number.")

        # As h_decode is equivalent to binascii.unhexlify for the
        # hexadecimal charset, this will work for both v1 and v2.
        return hexlify(cd.encrypt(data))

    def genQueryString(self
                       , authT = {}
                       , authZ = None
                       ):
        """Builds the query string with the McAuth data as returned by
        the authentication server.
        """

        if authZ is None:
            authZ_string = ""
        else:
            if len(authZ) == 0 or authZ.get("authz") is None:
                authZ['authz'] = []
            authZ_string = "&authz=%s" % (self.genAuthZToken(**authZ), )
        
        return "%s=%s%s" % (self.tokenName
                          , self.genAuthenticationToken(**authT)
                          , authZ_string
                          )

    def genAuthenticationToken(self
                               , session_id = 'sid1234567890'
                               , timestamp = None
                               , ip = '10.1.1.1'
                               , username = 'yoyoma'
                               , success = True):
        """Generates an authentication token for McAuth testing.
        """

        server_tag = {
            'v1': 'mcauth1.02',
            'v2': 'mcauth1.02',
            }[self.version]

        if timestamp is None:
            timestamp = int(time.time())
        elif not isinstance(timestamp, (str, int)):
            raise TypeError("The time must be an integer or string")

        success = {True: 'yes', False: 'no'}[success]

        return self.encodeData(":".join([server_tag,
                                         str(session_id),
                                         str(timestamp),
                                         ip,
                                         username,
                                         success
                                         ])
                               )

    def genAuthZToken(self
                      , authz
                      , authzEm = None
                      , authzStCrs = None
                      , authzStPgm = None
                      ):
        """Generates an authz token for McAuth v2 testing.

        If some parameter is None, no record of that type will be
        included in the token; if the value if an empty sequence, a
        default record will be inserted.
        """

        if len(authz) == 0:
            authz.append(self.getAuthzDict())

        if authzEm is None:
            authzEm = []
        elif isinstance(authzEm, list) and len(authzEm) == 0:
            authzEm.append(self.getAuthzEmDict())

        if authzStCrs is None:
            authzStCrs = []
        elif isinstance(authzStCrs, list) and len(authzStCrs) == 0:
            authzStCrs.append(self.getAuthzStCrsDict())

        if authzStPgm is None:
            authzStPgm = []
        elif isinstance(authzStPgm, list) and len(authzStPgm) == 0:
            authzStPgm.append(self.getAuthzStPgmDict())

        lines = []
        auth_data_set = filter( lambda (k, v): k in ('authz',
                                                     'authzEm',
                                                     'authzStCrs',
                                                     'authzStPgm'),
                                locals().items()
                                )
        for tag, records in auth_data_set:
            for record in records:
                value = ";".join( map( lambda ( k, v ): "%s:%s" % ( str(k), str(v) ),
                                       record.items()
                                       ))
                lines.append( "%s=%s" % (tag, value) )

        return self.encodeData( "!".join( lines ) )

    def getAuthzDict(self, **kwargs):
        """Return the default authz values.
        """

        default = {
            'referenceNo': 999999999,
            'applicIdNo':  9999999,
            'applicTypeCode': 'ST',
            'macIdNo': 'yoyoma',
            'aidActiveFlag': 'Y',
            'aidDormantFlag': '',
            'aidPreactiveFlag': 'Y',
            'surnameName': 'Ma',
            'initialsName': 'YYM',
            'fgivenameName': 'Yo Yo',
            'lgivenameName': 'Yo',
            'contactActiveFg': 'Y',
            'overdueAcctFlag': 'N',
            }


        default.update( dict( filter( None,
                                      map( lambda (k, v): default.has_key(k) and (k, v) or None,
                                           kwargs.items()
                                           ))))
        return default
    
    def getAuthzEmDict(self, **kwargs):
        """Returns the a default set of employee authz values.
        """

        default = {
            'referenceNo': 99999999,
            'positionCd': '',
            'macDeptCd': '',
            'macDeptDescTx': '',
            'jobCode': '',
            'jobDescriptionTx': '',
            'barcodeNo': '',
            'indvroletpCd': '',
            'emptpCd': '',
            'compGrpCd': '',
            }


        default.update( dict( filter( None,
                                      map( lambda (k, v): default.has_key(k) and (k, v) or None,
                                           kwargs.items()
                                           ))))
        return default
    
    def getAuthzStCrsDict(self, **kwargs):
        """Returns the a default set of student courses authz values.
        """

        default = {
            'referenceNo': 99999999,
            'studentNo': 9999999,
            'adminGroupCode': 'U',
            'subjectCode': '525',
            'courseCode': '2EL0',
            'startDateCode': 200509,
            'dayEveningFlag': 'E',
            'sectionCode': 'C02',
            'termCode': 1,
            'courseTitleText': 'CARER PLN. EXP LRN',
            'subjectText': 'SOC SCI',
            'labSectionCode': '',
            'tutSectionCode': '',
            'lecSectionCode': 'C02',
            }


        default.update( dict( filter( None,
                                      map( lambda (k, v): default.has_key(k) and (k, v) or None,
                                           kwargs.items()
                                           ))))
        return default
    
    def getAuthzStPgmDict(self, **kwargs):
        """Returns the a default set of student programs authz values.
        """

        default = {
            'referenceNo': 999999999,
            'studentNo': 9999999,
            'adminGroupCode': 'U',
            'startDateCode': 200509,
            'programCode': 1273,
            'levelCode': 5,
            'altProgramFlag': '',
            'programText': 'HEALTHST',
            'adminFacultyCode': 12,
            'adminFacultyText': 'SOC SCI',
            'fullPartFlag': 'P',
            'financialRegFlag': 'Y',
            }

        default.update( dict( filter( None,
                                      map( lambda (k, v): default.has_key(k) and (k, v) or None,
                                           kwargs.items()
                                           ))))
        return default

ZopeTestCase.installProduct('McAuthPlugin')
